from compradores.serializers import CompradorSerializer
from compradores.models import Comprador
from compradores.views import CompradoAPIView, CompradorList, VistaComprador
from django.urls import path


urlpatterns = [
    path('post-compra/', CompradoAPIView.as_view(), name='post-compra'),
    path('list-compra/', CompradorList.as_view(queryset=Comprador.objects.all(), serializer_class=CompradorSerializer)),
    path('details-comprador/<id>/', VistaComprador.as_view()),
    path('delete/<id>/', VistaComprador.as_view()),
]
