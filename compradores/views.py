from django.http.response import HttpResponse
from rest_framework import response
from compradores.serializers import CompradorSerializer
from compradores.models import Comprador
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework.permissions import IsAdminUser
import requests

# Create your views here.


class CompradoAPIView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        comprador = Comprador.objects.all()
        serializer = CompradorSerializer(comprador, many=True)
        return Response(serializer.data)

    def post(self, request):
    
        if not request.data[0]['longitud'] and not request.data[0]['latitud']:

            address = request.data[0]["direccion"].replace(" ", "+")
            city = request.data[0]["ciudad"]
            response = requests.get(
                'https://maps.googleapis.com/maps/api/geocode/json?address='+address+','+city+'&key=AIzaSyDlmeg0VOQzbGnYDlYaRapYCG6w_DmnB54')
            api_response_dict = response.json()
            latitude = api_response_dict['results'][0]['geometry']['location']['lat']
            longitude = api_response_dict['results'][0]['geometry']['location']['lng']

            new_data = [
                {
                    "usuario": request.data[0]['usuario'],
                    "nombre":request.data[0]['nombre'],
                    "apellido": request.data[0]['apellido'],
                    "direccion": request.data[0]['direccion'],
                    "ciudad": request.data[0]['ciudad'],
                    "latitud": latitude,
                    "longitud": longitude
                }
            ]
            print(new_data)
            serializer = CompradorSerializer(data=new_data, many=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CompradorList(generics.ListCreateAPIView):
    comprador = Comprador.objects.all()
    serializer_class = CompradorSerializer
    permission_classes = [IsAdminUser]

    def list(self, request):
        queryset = Comprador.objects.all()
        serializer = CompradorSerializer(queryset, many=True)
        return Response(serializer.data)

class VistaComprador(APIView):

    def get(self, request, id):
        comprador = Comprador.objects.filter(id=id)
        serializer = CompradorSerializer(comprador, many=True)
        return Response(serializer.data)

    def delete(self, request, id):
        comprador = self.kwargs["id"]
        comprador = get_object_or_404(Comprador, id=id)
        comprador.delete()
        return Response(status=204)
    
    