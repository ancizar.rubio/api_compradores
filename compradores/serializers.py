from compradores.models import Comprador
from rest_framework import fields, serializers


class CompradorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comprador
        fields = ['usuario', 'nombre', 'apellido', 'direccion'
                  ,'ciudad', 'longitud', 'latitud']