from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Comprador(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    nombre = models.CharField(verbose_name="Nombre", max_length=50)
    apellido = models.CharField(verbose_name="Apellido", max_length=50)
    direccion = models.CharField(verbose_name="Dirección", max_length=50)
    ciudad = models.CharField(verbose_name="Ciudad", max_length=50)
    longitud = models.CharField(verbose_name="Longitud", max_length=50, null=True, blank=True)
    latitud = models.CharField(verbose_name="Latitud", max_length=50, null=True, blank=True)
    estado_geo = models.CharField( max_length=20, blank=True, null=True,)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    
    class Meta:
        verbose_name="Comprador"
        verbose_name_plural = "Compradores"
        ordering = ['-id']
        
    def __str__(self):
        return self.nombre

    
   
