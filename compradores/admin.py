from django.db import models
from compradores.models import Comprador
from django.contrib import admin

# Register your models here.

class CompradorAdmin(admin.ModelAdmin):
    readonly_fields = ('estado_geo','created')
    list_display = ('nombre', 'apellido', 'direccion', 'ciudad')

admin.site.register(Comprador, CompradorAdmin)